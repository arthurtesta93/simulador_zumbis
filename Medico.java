import java.util.List;
import java.util.stream.Collectors;

public class Medico extends Personagem{
    private static int chanceCura = 70;

    public Medico(int linInicial, int colInicial) {
        super(10, "Medico", linInicial, colInicial);
    }
    
	public Medico(int energiaInicial, String imagemInicial, int linInicial, int colInicial) {
		super(energiaInicial, imagemInicial, linInicial, colInicial);

	}
	
    public Medico(int pv,int linInicial, int colInicial, int blockCount, boolean infectado) {
        super(pv, "Medico", linInicial, colInicial);
        this.setBlockCount(blockCount);
        if(infectado) {
        	this.infecta();
        }
    }
	
    public void infecta(){
        if (this.infectado()){
            return;
        }
        super.infecta();
        this.setImage("Infectado");
        this.getCelula().setImageFromPersonagem();   
    }
    
    @Override
    public void cura(){
        if (!this.infectado()){
            return;
        }
        super.cura();
        this.setImage("Medico");
        this.getCelula().setImageFromPersonagem();   
    }

    /**
     * Busca personagem zumbi mais proximo e define como alvo.
     * @return personagem alvo
     */
    public void defineAlvo(){
        double distAlvo = Double.MAX_VALUE;
        double dist;
        List<Personagem> lstP = Jogo.getInstance().getPersonagens();
        lstP = lstP.stream().filter(p->p.estaVivo()).filter(p->p instanceof Bobao || p instanceof Combatente).collect(Collectors.toList());
        for (Personagem personagem : lstP) {
            dist = getDistancia(personagem);
            if(dist < distAlvo){
                setAlvo(personagem);
                distAlvo = dist;
            }
        }
    }
    /**
     * Move na direcao do alvo
     */
    @Override
    public void atualizaPosicao() {
        if(!estaVivo()){
            return;
        }
        if(getAlvo()==null||getBlockCount()>getMaxBlock()){
            super.atualizaPosicao();
            clearBlockCount();
            return;
        }

        int oldLin = this.getCelula().getLinha();
        int oldCol = this.getCelula().getColuna();

        int linAlvo = super.getAlvo().getCelula().getLinha();
        int colAlvo = super.getAlvo().getCelula().getColuna();

        int lin = oldLin;
        int col = oldCol;
        if(lin < linAlvo) lin++;
        if(lin > linAlvo) lin--;
        if(col > colAlvo) col--;
        if(col < colAlvo) col++;


        if (lin < 0) lin = 0;
        if (lin >= Jogo.NLIN) lin = Jogo.NLIN-1;
        if (col < 0) col = 0;
        if (col >= Jogo.NCOL) col = Jogo.NCOL-1;

        if (Jogo.getInstance().getCelula(lin, col).getPersonagem() != null){
            super.incrBlockCount();
            return;
        }else{
            // Limpa celula atual
            Jogo.getInstance().getCelula(oldLin, oldCol).setPersonagem(null);
            // Coloca personagem na nova posição
            Jogo.getInstance().getCelula(lin, col).setPersonagem(this);
            super.clearBlockCount();
        }
    }

	@Override
	public void influenciaVizinhos() {
        if(!estaVivo()){
            return;
        }
        	if(getAlvo()!=null&&getDistancia(getAlvo())<2){ //true se esta em uma celula adjacente
            if(diceRoller(chanceCura)){
                System.out.println("M�dico ("+this.getCelula().getLinha()+","+this.getCelula().getColuna()+") prestou socorro a " +getAlvo().getClass().getName()+" ("+getAlvo().getCelula().getLinha()+","+getAlvo().getCelula().getColuna()+")");
                getAlvo().incrementaEnergia(2);
                if(diceRoller(getChanceCura())){
                    System.out.println("M�dico ("+this.getCelula().getLinha()+","+this.getCelula().getColuna()+") curou " +getAlvo().getClass().getName()+" ("+getAlvo().getCelula().getLinha()+","+getAlvo().getCelula().getColuna()+")");
                    getAlvo().cura();
                }
            }      
        }
    }

	
    @Override
    public void verificaEstado() {
        // Se esta infectado perde energia a cada passo
        if (this.infectado()) {
            diminuiEnergia(2);
            // Se não tem mais energia morre
            if (this.getEnergia() == 0) {
                this.setImage("Morto");
                this.getCelula().setImageFromPersonagem();
            }
        }
        super.verificaEstado();
    }
}
	
	
 
