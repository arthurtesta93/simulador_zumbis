
import java.awt.Color;
import java.awt.Desktop.Action;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Scanner;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Jogo extends Application {
    private HBox hb = new HBox(10);
    public static final int CELL_WIDTH = 20;
    public static final int CELL_HEIGHT = 20;
    public static final int NLIN = 12;
    public static final int NCOL = 12;
    private static double progress;
    private boolean dia = true;
    private double turnCounter;
    private final int maxTurn = 40;
    public static Jogo jogo = null;
    private ProgressBar pb;
    private TextField nmrBoboes;
    private TextField nmrCombatentes;
    private TextField nmrMedicos;
    private TextField nmrZumbi;
    private TextField nmrZEsperto;
    private Random random;
    private Map<String, Image> imagens;
    private List<Celula> celulas;
    private List<Personagem> personagens;
    private Stage customizationStage;
    
    /* Simulador de Zumbis - Programa��o Orientada a Objetos
     * Professor: Bernardo Copstein
     * Desenvolvedores: Micael Fischmann e Arthur Testa
     */

    public static Jogo getInstance(){
        return jogo;
    }
    /**
     * @return  personagens atualmente no jogo
     */
    public List<Personagem> getPersonagens() {
        return personagens;
    }
    public Jogo(){
        jogo = this;
        pb = new ProgressBar();
        random = new Random();
     
    }

    public static void main(String[] args) {
        launch(args);
    }

    // Retorna um número aleatorio a partir do gerador unico
    public int aleatorio(int limite){
        return random.nextInt(limite);
    }

    // Retorna a celula de uma certa linha,coluna
    public Celula getCelula(int nLin,int nCol){
        int pos = (nLin*NCOL)+nCol;
        return celulas.get(pos);
    }

    private void loadImagens() {
        imagens = new HashMap<>();

        // Armazena as imagens dos personagens
        Image aux = new Image("file:Imagens\\img1.jpg");
        imagens.put("Normal", aux);
        aux = new Image("file:Imagens\\img10.png");
        imagens.put("Cidadao", aux);
        aux = new Image("file:Imagens\\img2.jpg");
        imagens.put("Infectado", aux);
        aux = new Image("file:Imagens\\img15.png");
        imagens.put("Zumbi", aux);
        aux = new Image("file:Imagens\\img11.png");
        imagens.put("Esperto", aux);
        aux = new Image("file:Imagens\\img12.png");
        imagens.put("Morto", aux);
        aux = new Image("file:Imagens\\img13.png");
        imagens.put("Medico", aux);
        aux = new Image("file:Imagens\\img14.png");
        imagens.put("Combatente", aux);
        aux = new Image("file:Imagens\\citizen.jpg");
        imagens.put("portraitBobao", aux);
        aux = new Image("file:Imagens\\police.jpg");
        imagens.put("portraitCombatente", aux);
        aux = new Image("file:Imagens\\doctor.jpg");
        imagens.put("portraitMedico", aux);
        aux = new Image("file:Imagens\\zumbi.jpg");
        imagens.put("portraitZumbi", aux);
        aux = new Image("file:Imagens\\Ghoul.png");
        imagens.put("portraitZumbiEsperto", aux);
        aux = new Image("file:Imagens\\back.jpg");
        imagens.put("Vazio", aux);

        // Armazena a imagem da celula ula
        imagens.put("Null", null);
    }

    public Image getImage(String id){
        return imagens.get(id);
    }
    /**
     * Janela menu inicial
     */
    @Override
    public void start(Stage setUpStage) {
        setUpStage.setTitle("Configuracao Simulacao");
        //Cria os botoes
        Button deafultButton = new Button("Jogo Default");
        Button customButton = new Button("Jogo Customizado");
        Button loadButton = new Button("Carregar Jogo");

        //Define o que os botoes fazem
        deafultButton.setOnAction(e->criaSimulacao(5,3,2,4,2));//Numeros default
        customButton.setOnAction(e->customSimulacao());
        loadButton.setOnAction(e->carregaJogo());

        HBox menuH = new HBox(10);
        //Adiciona botoes a caixa horizontal
        menuH.getChildren().add(deafultButton);
        menuH.getChildren().add(customButton);
        menuH.getChildren().add(loadButton);
        Text contexto = new Text("Um virus contagiou o sistema de agua de uma cidade, transformando humanos em "+
        "zumbis, e foi colocada em quarentena pelas forcas armadas. A cidade ficara fechada durante "+
        "este periodo enquanto cientistas trabalham em uma cura. Cabe aos cidadaos sobreviverem o "+
        "ataque dos zumbis por 24 horas, ou ate a ameaca desparecer.\n\n"+
        "Segundo informacoes do governo, a cidade no momento e composta 40% por zumbis, e 60% "+
        "por humanos. Entre os humanos, metade sao indefesos, 30% sao medicos e 20% sao "+
        "combatentes. Os zumbis sao divididos entre caminhantes (se movem aleatoriamente) e "+
        "perseguidores (se movem em direcao a um alvo).\n\n"+
        "Os zumbis atacam os humanos proximos, e os combatentes atacam os zumbis a distancias curtas. "+
        "Quando infectados, os humanos perdem pontos de vida com o passar do tempo. Medicos "+
        "podem recuperar pontos de vida e, em alguns casos, curar humanos infectados.");
        contexto.setWrappingWidth(800);
        GridPane gridMenu = new GridPane();
        gridMenu.setAlignment(Pos.CENTER);
        gridMenu.setHgap(10);
        gridMenu.setVgap(10);
        gridMenu.setPadding(new Insets(25, 25, 25, 25));
        gridMenu.add(contexto, 0, 0);
        gridMenu.add(menuH,0,1);
        Scene sceneMenu = new Scene(gridMenu);
        setUpStage.setScene(sceneMenu);
        setUpStage.show();
    }
    /**
     * Janela para entrada de dados para inicializar jogo custom
     */
    public void customSimulacao(){
        customizationStage = new Stage();
        customizationStage.setTitle("Tela Customizacao");
        GridPane customizationGrid = new GridPane();
        customizationGrid.setAlignment(Pos.CENTER);
        customizationGrid.setHgap(10);
        customizationGrid.setVgap(10);
        customizationGrid.setPadding(new Insets(25, 25, 25, 25));
        Text info = new Text("Digite numero de cada personagem (0 a 10):");
        customizationGrid.add(info,0,0);

        Label labelNmrBoboes = new Label("Quantidade de cidadoes normais.");
        nmrBoboes = new TextField();
        customizationGrid.add(labelNmrBoboes,0,1);
        customizationGrid.add(nmrBoboes,1,1);

        Label labelNmrCombatentes = new Label("Quantidade de combatentes.");
        nmrCombatentes = new TextField();
        customizationGrid.add(labelNmrCombatentes,0,2);
        customizationGrid.add(nmrCombatentes,1,2);

        Label labelNmrMedicos = new Label("Quantidade de medicos.");
        nmrMedicos = new TextField();
        customizationGrid.add(labelNmrMedicos,0,3);
        customizationGrid.add(nmrMedicos,1,3);

        Label labelNmrZumbi = new Label("Quantidade de zumbis normais.");
        nmrZumbi = new TextField();
        customizationGrid.add(labelNmrZumbi,0,4);
        customizationGrid.add(nmrZumbi,1,4);

        Label labelNmrZEsperto = new Label("Quantidade de zumbis espertos.");
        nmrZEsperto = new TextField();
        customizationGrid.add(labelNmrZEsperto,0,5);
        customizationGrid.add(nmrZEsperto,1,5);

        Button validaCustomizacao = new Button("Confirma");
        validaCustomizacao.setOnAction(e->readCustomizacao(e));
        customizationGrid.add(validaCustomizacao,0,6);

        Scene customizationScene = new Scene(customizationGrid);
        customizationStage.setScene(customizationScene);
        customizationStage.show();
    }
    /**
     * Le input de usuarios para inicializar jogo
     */
    public void readCustomizacao(javafx.event.ActionEvent e) {
        try {
            ArrayList<Integer> qtds = new ArrayList<>();
            qtds.add(Integer.parseInt(nmrBoboes.getText()));
            qtds.add(Integer.parseInt(nmrCombatentes.getText()));
            qtds.add(Integer.parseInt(nmrMedicos.getText()));
            qtds.add(Integer.parseInt(nmrZumbi.getText()));
            qtds.add(Integer.parseInt(nmrZEsperto.getText()));

            for (Integer integer : qtds) {
                if(integer<0 || integer>10){
                    throw new IllegalArgumentException();
                }
            }
            customizationStage.close();
            criaSimulacao(qtds.get(0), qtds.get(1), qtds.get(2), qtds.get(3), qtds.get(4));
        } catch (Exception ex) {
            Alert errorBox = new Alert(AlertType.INFORMATION);
            errorBox.setHeaderText("Erro de entrada");
            errorBox.setContentText("Favor digite apenas numeros de 0 a 10.");
            errorBox.showAndWait();
        }

    }
     /* Inicializa jogo
     * @param qtdBoboes numero inicial de Boboes
     * @param qtdCombatentes numero inicial de Combatentes
     * @param qtdMedicos numero inicial de Medicos
     * @param qtdZumbis numero inicial de Zumbis
     * @param qtdZumbisEspertos numero inicial de ZumbisEspertos
     */
    public void criaSimulacao(int qtdBoboes, int qtdCombatentes, int qtdMedicos, int qtdZumbis, int qtdZumbisEspertos){
        Stage primaryStage = new Stage();
        // Carrega imagens
        loadImagens();

        // Configura a interface com o usuario
        primaryStage.setTitle("Simulador de Zumbis");
        GridPane tab = new GridPane();
        tab.setAlignment(Pos.CENTER);
        tab.setHgap(10);
        tab.setVgap(10);
        tab.setPadding(new Insets(25, 25, 25, 25));

        // Monta o "tabuleiro"
        celulas = new ArrayList<>(NLIN*NCOL);
        for (int lin = 0; lin < NLIN; lin++) {
            for (int col = 0; col < NCOL; col++) {
                Celula cel = new Celula(lin,col);
                cel.setOnAction(e->cliqueNaCelula(e));
                celulas.add(cel);
                tab.add(cel, col, lin);
            }
        }

        // Cria a lista de personagens
        personagens = new ArrayList<>(NLIN*NCOL);
        /*
        personagens.add(new ZumbiEsperto(2,2));
        personagens.add(new Bobao(2,5));
        personagens.add(new Bobao(2,8));
        personagens.add(new Bobao(6,2));
        personagens.add(new Combatente(8,8));
        personagens.add(new Zumbi(1,1));*/
        // Cria boboes aleatorios
        for(int i=0;i<qtdBoboes;i++){
            // Lembrte: quando um personagem é criado ele se vincula
            // automaticamente na célula indicada nos parametros
            // linha e coluna (ver o construtor de Personagem)
            boolean posOk = false;
            while(!posOk){
                int lin = random.nextInt(NLIN);
                int col = random.nextInt(NCOL);
                if (this.getCelula(lin, col).getPersonagem() == null){
                    personagens.add(new Bobao(lin,col));
                    posOk = true;
                }
            }
        }
        // Cria Combatentes aleatorios
        for(int i=0;i<qtdCombatentes; i++){
            // Lembrte: quando um personagem é criado ele se vincula
            // automaticamente na célula indicada nos parametros
            // linha e coluna (ver o construtor de Personagem)
            boolean posOk = false;
            while(!posOk){
                int lin = random.nextInt(NLIN);
                int col = random.nextInt(NCOL);
                if (this.getCelula(lin, col).getPersonagem() == null){
                    personagens.add(new Combatente(lin,col));
                    posOk = true;
                }
            }
        }
        for(int i=0;i<qtdMedicos; i++){ 
            boolean posOk = false;
            while(!posOk){
                int lin = random.nextInt(NLIN);
                int col = random.nextInt(NCOL);
                if (this.getCelula(lin, col).getPersonagem() == null){
                    personagens.add(new Medico(lin,col));
                    posOk = true;
                }
            }
        }
        // Cria Zumbis aleatórios
        for(int i=0;i< qtdZumbis;i++){
            boolean posOk = false;
            while(!posOk){
                int lin = random.nextInt(NLIN);
                int col = random.nextInt(NCOL);
                if (this.getCelula(lin, col).getPersonagem() == null){
                    personagens.add(new Zumbi(lin,col));
                    posOk = true;
                }
            }
        }
        // Cria ZumbisEspertos aleatórios
        for(int i=0;i< qtdZumbisEspertos;i++){
            boolean posOk = false;
            while(!posOk){
                int lin = random.nextInt(NLIN);
                int col = random.nextInt(NCOL);
                if (this.getCelula(lin, col).getPersonagem() == null){
                    personagens.add(new ZumbiEsperto(lin,col));
                    posOk = true;
                }
            }
        }
        // Define o botao que avança a simulação
        Button avanca = new Button("NextStep");
        avanca.setOnAction(e->avancaSimulacao());
        // Define outros botoes
        Button saveGame = new Button("Save Game");
        saveGame.setOnAction(e->jogo.salvaJogo());
        
        // Monta a cena e exibe
        hb.setAlignment(Pos.CENTER);
        hb.setPadding(new Insets(25, 25, 25, 25));
        hb.getChildren().add(tab);

        VBox botoesV = new VBox(10);
        HBox hbotoes = new HBox(10);
        botoesV.getChildren().add(avanca);
        botoesV.getChildren().add(saveGame);
        Text turnInfo = new Text("Progresso para cura");
        pb.setProgress(0);
        VBox turnPB = new VBox(10);
        turnPB.getChildren().add(turnInfo);
        turnPB.getChildren().add(pb);
        hbotoes.getChildren().add(turnPB);
        hbotoes.getChildren().add(avanca);
        hbotoes.getChildren().add(saveGame);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        grid.add(hb,0,1);
        grid.add(hbotoes,0,0);
        

        Scene scene = new Scene(grid);
        primaryStage.setScene(scene);
        primaryStage.show();
        
    }
    
    public boolean diaOuNoite(){
    	Alert alert = new Alert(AlertType.WARNING);
    	
    	if(turnCounter ==  20) {
    		dia = false;
    		alert.setContentText("Caiu a noite... a chance dos m�dicos curarem caiu pela metade!");
    		alert.showAndWait();
    		return false;
    	}
    	return true;
    }
    
    
    /**
     * Ativa acoes de personagens e confere win/lose conditions
     */
    public void avancaSimulacao(){
        // Avança um passo em todos os personagens
        turnCounter++;
        progress = turnCounter/maxTurn;
        pb.setProgress(progress);
        personagens.stream().filter(p->p.estaAtivo()).forEach(p->{
            p.defineAlvo();
            p.atualizaPosicao();
            p.verificaEstado();
            p.influenciaVizinhos();
        });
        System.out.println("Turno: "+(int) turnCounter);
        diaOuNoite();
        System.out.println(dia);
        if (!dia) {
        	hb.setStyle("-fx-background-color: #200c07");
        	Personagem.setChanceCura(20);
        }        
        // Verifica se o jogo acabou
        long boboesVivos = personagens
                    .stream()
                    .filter(p->!(p instanceof Zumbi))
                    .filter(p->p.estaVivo())
                    .count();

        long zumbisVivos = personagens
                    .stream()
                    .filter(p->(p instanceof Zumbi))
                    .filter(p->p.estaVivo())
                    .count();
        if(turnCounter >= maxTurn || zumbisVivos == 0){
            Alert msgBox = new Alert(AlertType.INFORMATION);
            msgBox.setHeaderText("Fim de Jogo");
            msgBox.setContentText("Vitoria dos cidadoes!");
            msgBox.showAndWait();
            System.exit(0);
        }
        if (boboesVivos == 0){
            Alert msgBox = new Alert(AlertType.INFORMATION);
            msgBox.setHeaderText("Fim de Jogo");
            msgBox.setContentText("Todos os boboes morreram!");
            msgBox.showAndWait();
            System.exit(0);
        }
    }
    
    public void carregaJogo() {
    	loadImagens(); // carrega HashMap de imagens
    	criaSimulacao(0,0,0,0,0);  // inicia o stage do tabuleiro de jogo, sem personagens
        String currDir = Paths.get("").toAbsolutePath().toString();
        File dir = new File(currDir);
        String fileName = null;
        FilenameFilter filter = new FilenameFilter() {
        	public boolean accept (File dir, String name) {
        		return name.contains(".txt");
        	}
        };
        List<String> choices = new ArrayList<>();
        String[] children = dir.list(filter);
        if(children == null) {
        	Alert alert = new Alert(AlertType.WARNING);
        	alert.setTitle("Jogos Salvos");
        	alert.setHeaderText(null);
        	alert.setContentText("N�o existem jogos salvos.");
        	alert.showAndWait();
        } else {
        	for(int i = 0; i < children.length; i++) {
        		String filename = children[i];
        		choices.add(filename);
        	}
        }
		ChoiceDialog<String> dialog = new ChoiceDialog<>("", choices);
		dialog.setTitle("Jogos Salvos");
		dialog.setHeaderText("Jogos Salvos");
		dialog.setContentText("Escolha o jogo salvo:");
		
		Optional<String> result = dialog.showAndWait();
		
		if (result.isPresent()){
		    fileName = result.get();
		}
        String nameComplete = currDir + "\\" + fileName;
        Path path = Paths.get(nameComplete);
        
        try (Scanner sc = new Scanner(Files.newBufferedReader(path, StandardCharsets.UTF_8))){
        	 sc.useDelimiter(",|\\n");
        	 sc.useLocale(Locale.ENGLISH);
        	 System.out.println("Carregando arquivo salvo em: " + path.getFileName());
        	 while(sc.hasNext()) {

     			String nomePersonagem = sc.next();
        		int energia = sc.nextInt();
        		boolean infectado = sc.nextBoolean();
        		int linha = sc.nextInt();
        		int coluna = sc.nextInt();
        		int blockCount = sc.nextInt();
        		double turnoAtual = sc.nextDouble();
        		System.out.println("Carregando personagem");  
        		
        		Celula c = new Celula(linha, coluna);
        		celulas.add(c);
        		
        		        		
        		switch(nomePersonagem) {
        			case "Cidadao":
        				personagens.add(new Bobao(energia, linha, coluna, blockCount, infectado));
                		break;
                		
        			case "Combatente":
        				personagens.add(new Combatente(energia, linha, coluna, blockCount, infectado));
        				break;
        				
        			case "Zumbi":
        				personagens.add(new Zumbi(linha, coluna));
        				break;
        				
        			case "Esperto":
        				personagens.add(new ZumbiEsperto(energia, linha, coluna));
        				break;
        				
        			case "Medico":
        				personagens.add(new Medico(energia, linha, coluna, blockCount, infectado));
        				break; 
        				
        			case "Morto":
        				break;
        		}
        		this.turnCounter = turnoAtual;
        	 }
        	 	sc.close();
        	 
        } catch (IOException x) {
        	System.err.format("Erro na leitura do arquivo salvo", x);
        } 
         
    }

    public void salvaJogo() {
    	TextInputDialog dialog = new TextInputDialog("Jogo Salvo 1");
    	dialog.setTitle("Salvar Jogo");
    	dialog.setHeaderText("Salvar Jogo");
    	dialog.setContentText("Por favor, insira o nome do arquivo de jogo:");
    	Optional<String> result = dialog.showAndWait();
    	if(result.isPresent()) {
    	String currDir = Paths.get("").toAbsolutePath().toString();
    	String nameComplete = currDir + "\\" + result.get() +".txt";
    	Path saveGame = Paths.get(nameComplete);
    	
    	try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(saveGame, StandardCharsets.UTF_8))){
    		int coluna, linha;
    		List<Personagem> listaJogadores = getPersonagens();
    		for(Personagem p : listaJogadores) {
    			linha = p.getCelula().getLinha();
    			coluna = p.getCelula().getColuna();
    			writer.print(p.getImage() + ",");
    			writer.print(p.getEnergia() +",");
    			writer.print(p.infectado()+",");
    			writer.print(linha + ",");
    			writer.print(coluna + ",");
    			writer.print(p.getBlockCount() + ",");
    			writer.print(turnCounter + "\n");
    			System.out.println("Personagem salvo com sucesso");
    		}
    		
    	} catch (IOException e) {
			System.err.format("Erro na gravacao do jogo", e);
    	}
		System.out.println("Jogo salvo com sucesso");
    }
  }
    /**
     * Cria janela com informacoes do porsonagem ao clicar na celula que ele ocupa
     * @param e evento de clicar na celula
     */
    public void cliqueNaCelula(javafx.event.ActionEvent e) {
        Celula c = (Celula) e.getSource();
        Personagem p = c.getPersonagem();
        if(p!=null){
            Stage characterSheet = new Stage();
            characterSheet.setTitle("Ficha de Personagem");
            HBox vida = new HBox(10);
            Text descricao = new Text(p.getClass().getName());
            Text descricaoVida = new Text("Pontos de vida: ");
            Text pv = new Text(p.getEnergia()+"/"+p.getPvMax());
            vida.getChildren().add(descricaoVida);
            vida.getChildren().add(pv);
            GridPane csGrid = new GridPane();
            csGrid.setAlignment(Pos.CENTER);
            csGrid.setHgap(10);
            csGrid.setVgap(10);
            csGrid.setPadding(new Insets(25, 25, 25, 25));
            csGrid.add(descricao, 0, 1);
            csGrid.add(vida, 0, 2);
            if(p instanceof Bobao){
                Text status = new Text("Infectado: ");
                Text statusInfectado = new Text();
                if(p.infectado()){
                    statusInfectado.setText("Sim");
                }
                else{
                    statusInfectado.setText("Nao");
                }
                HBox infect = new HBox(10);
                infect.getChildren().add(status);
                infect.getChildren().add(statusInfectado);
                csGrid.add(infect, 0, 3);
            }
            String nome = "portrait"+p.getClass().getName();
            Image portrait = imagens.get(nome); 
            ImageView portraitView = new ImageView(portrait);
            portraitView.setFitWidth(200);
            portraitView.setFitHeight(200);
            csGrid.add(portraitView, 0, 0);
            Scene csScene = new Scene(csGrid);
            characterSheet.setScene(csScene);
            characterSheet.show();
        }
    }
}
