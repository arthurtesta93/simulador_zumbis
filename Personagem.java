import java.util.Random;

public abstract class Personagem {
	
    private static int chanceCura = 50;
    private String imagem; // Identificador da imagem
    private int energia;
    private boolean infectado;
    private Celula celula;
    private int blockCount;
    private int deathCount;
    private final int dispawn = 1;
    private final int maxCount = 19; //numero maximo p tentar mover aleatorio
    private final int range = 100;
    private static final int maxBlock = 2;
    private Personagem alvo;
    private boolean ativo;
    private int pvMax = 10;

    public Personagem(int energiaInicial, String imagemInicial,int linInicial,int colInicial){
        this.imagem = imagemInicial;
        this.energia = energiaInicial;
        Jogo.getInstance().getCelula(linInicial, colInicial).setPersonagem(this);
        this.infectado = false;
        blockCount = 0;
        alvo = null;
        ativo = true;
    }
    
    public int getChanceCura() {
        return chanceCura;
    }

    public static void setChanceCura(int novaChance) {
        chanceCura = novaChance;
    }

    public int getEnergia(){
        return energia;
    }
    /**
     * @return the pvMax
     */
    public int getPvMax() {
        return pvMax;
    }

    /**
     * @return the alvo
     */
    public Personagem getAlvo() {
        return alvo;
    }

    /**
     * @return the maxBlock
     */
    public int getMaxBlock() {
        return maxBlock;
    }

    /**
     * @param alvo the alvo to set
     */
    public void setAlvo(Personagem alvo) {
        this.alvo = alvo;
    }

    public double getDistancia(Personagem alvo){
        if(alvo==null){
            return 0;
        }
        int colOrig = this.getCelula().getColuna();
        int linOrig = this.getCelula().getLinha();
        
        int colDest = alvo.getCelula().getColuna();
        int linDest = alvo.getCelula().getLinha();

        double dist = Math.sqrt(Math.pow(colDest-colOrig, 2)+Math.pow(linDest-linOrig, 2));
        return dist;
    }

    public void incrementaEnergia(int valor){
        if (valor < 0) throw new IllegalArgumentException("Valor de energia invalido");
        energia += valor;
    }

    public void diminuiEnergia(int valor){
        if (valor < 0) throw new IllegalArgumentException("Valor de energia invalido");
        energia -= valor;
        if (energia < 0){
            energia = 0;
        }
    }

    public boolean infectado(){
        return infectado;
    }

    public void infecta(){
        infectado = true;
    }

    public void cura(){
        infectado = false;
    }

    public boolean estaVivo(){
        return getEnergia() > 0;
    }

    public boolean estaAtivo(){
        return ativo;
    }

    public String getImage(){
        return imagem;
    }

    public void incrBlockCount(){
        blockCount++;
    }

    public int getBlockCount(){
        return blockCount;
    }

    public void clearBlockCount(){
        blockCount = 0;
    }

    public void setImage(String imagem){
        this.imagem = imagem;
    }

    public Celula getCelula(){
        return celula;
    }

    public void setCelula(Celula celula){
        this.celula = celula;
    }

    // Define próximo movimento. Default aleatorio p Personagem
    public void atualizaPosicao(){
        if (!this.estaVivo()){
            return;
        }
        int oldLin = this.getCelula().getLinha();
        int oldCol = this.getCelula().getColuna();
        int lin = oldLin;
        int col = oldCol;
        int count = 0;
        do{
            int dirLin = Jogo.getInstance().aleatorio(3)-1;
            int dirCol = Jogo.getInstance().aleatorio(3)-1;
            lin = oldLin + dirLin;
            col = oldCol + dirCol;
            if (lin < 0) lin = 0;
            if (lin >= Jogo.NLIN) lin = Jogo.NLIN-1;
            if (col < 0) col = 0;
            if (col >= Jogo.NCOL) col = Jogo.NCOL-1;
            count++;
            if(count>maxCount){
                return;
            }
        }
        while(Jogo.getInstance().getCelula(lin, col).getPersonagem() != null);
    
        // Limpa celula atual
        Jogo.getInstance().getCelula(oldLin, oldCol).setPersonagem(null);
        // Coloca personagem na nova posição
        Jogo.getInstance().getCelula(lin, col).setPersonagem(this);
    }

    // Verifica possiveis atualizações de estado a cada passo
    public void verificaEstado(){

        if (this.getEnergia() == 0) {
            this.setImage("Morto");
            this.getCelula().setImageFromPersonagem();
        }

        if (!this.estaVivo()){
            deathCount++;
            if(deathCount>dispawn){        
                this.getCelula().setPersonagem(null);
                ativo = false;
            }
            return;
        }
    }
    
    public void setBlockCount(int blockCounter){
    	
    	this.blockCount = blockCounter;
    	
    }
    
    /**
     * Simulador de dado d100
     * @param baseChance target para o dado
     * @return true se dado e maior ou igual a chance base e false caso contrario
     */
    public boolean diceRoller(int baseChance){
        Random rand = new Random();
        int diceRoll = rand.nextInt(range); //numero aleatorio de 1 a 100
        if(diceRoll>baseChance){
            return true;
        }
        return false;
    }
    // Define como o personagem influencia os vizinhos
    // Toda vez que chega em uma célula analisa os vizinhos
    // e influencia os mesmos
    public abstract void influenciaVizinhos();

    //Define como personagem define o Alvo, o que determina movimento e influencia de vizinhos
    public abstract void defineAlvo();
}
