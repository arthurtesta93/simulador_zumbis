public class Combatente extends Bobao{
    private static int rangeArma = 3;
    private static int chanceHit = 60;
    public Combatente(int linInicial, int colInicial){
        super(10, "Combatente", linInicial, colInicial);
    }

    public Combatente(int pv, int linInicial, int colInicial){
        super(pv, "", linInicial, colInicial);
    }
    
    public Combatente(int pv, int linInicial, int colInicial, int blockCount, boolean infectado){
        super(10, "Combatente", linInicial, colInicial);
        this.setBlockCount(blockCount);
        if(infectado) {
        	this.infecta();
        }
    }

    /**
     * @return the rangeArma
     */
    public static int getRangeArma() {
        return rangeArma;
    }

    /**
     * @param rangeArma the rangeArma to set
     */
    public static void setRangeArma(int rangeArma) {
        Combatente.rangeArma = rangeArma;
    }

    @Override //Move na direcao do alvo
    public void atualizaPosicao() {
        if(!estaVivo()){
            return;
        }
        if(getAlvo()==null||getBlockCount()>getMaxBlock()){
            super.atualizaPosicao();
            clearBlockCount();
            return;
        }

        if(getDistancia(getAlvo()) < Combatente.rangeArma-1){ //Se esta muito proximo tenta pegar distancia
            super.atualizaPosicao();
            return;
        }

        int oldLin = this.getCelula().getLinha();
        int oldCol = this.getCelula().getColuna();

        int linAlvo = getAlvo().getCelula().getLinha();
        int colAlvo = getAlvo().getCelula().getColuna();

        int lin = oldLin;
        int col = oldCol;
        if(lin < linAlvo) lin++;
        if(lin > linAlvo) lin--;
        if(col > colAlvo) col--;
        if(col < colAlvo) col++;


        if (lin < 0) lin = 0;
        if (lin >= Jogo.NLIN) lin = Jogo.NLIN-1;
        if (col < 0) col = 0;
        if (col >= Jogo.NCOL) col = Jogo.NCOL-1;

        if (Jogo.getInstance().getCelula(lin, col).getPersonagem() != null){
            incrBlockCount();
            return;
        }else{
            // Limpa celula atual
            Jogo.getInstance().getCelula(oldLin, oldCol).setPersonagem(null);
            // Coloca personagem na nova posição
            Jogo.getInstance().getCelula(lin, col).setPersonagem(this);
            clearBlockCount();
        }
    }

    @Override
    public void influenciaVizinhos() {
        if(!estaVivo()){
            return;
        }
        if(getAlvo()!=null&&getDistancia(getAlvo())<=rangeArma){ //true se esta dentro do range da arma
            if(diceRoller(chanceHit)){
                getAlvo().diminuiEnergia(3);
                System.out.println("Combatente ("+this.getCelula().getLinha()+","+this.getCelula().getColuna()+") acertou " +getAlvo().getClass().getName()+" ("+getAlvo().getCelula().getLinha()+","+getAlvo().getCelula().getColuna()+")");
            }      
        }
    }
}