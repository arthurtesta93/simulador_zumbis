import java.util.List;
import java.util.stream.Collectors;

public class ZumbiEsperto extends Zumbi {
    private static int chanceHit = 70;
    public ZumbiEsperto(int linInicial,int colInicial){
        super("Esperto",linInicial,colInicial);
    }
    public ZumbiEsperto(int pv, int linInicial,int colInicial){
        super(pv, "Esperto",linInicial,colInicial);
    }
    /**
     * @return the chanceHit
     */
    public static int getChanceHit() {
        return chanceHit;
    }

    /**
     * @param chanceHit the chanceHit to set
     */
    public static void setChanceHit(int chanceHit) {
        ZumbiEsperto.chanceHit = chanceHit;
    }

    /**
     * Busca personagem nao zumbi nao infectado mais proximo e define como super.getAlvo().
     * @return personagem alvo
     */
    @Override
    public void defineAlvo(){
        double distAlvo = Double.MAX_VALUE;
        double dist;
        super.setAlvo(null);
        List<Personagem> lstP = Jogo.getInstance().getPersonagens();
        lstP = lstP.stream().filter(p->p.estaVivo()).filter(p->p instanceof Bobao || p instanceof Medico).collect(Collectors.toList());
        for (Personagem personagem : lstP) {
            dist = getDistancia(personagem);
            if(dist < distAlvo){
                super.setAlvo(personagem);
                distAlvo = dist;
            }
        }
    }
    @Override
    public void atualizaPosicao() {
        if(!estaVivo()){
            return;
        }
        if(getAlvo()==null||getBlockCount()>getMaxBlock()){
            super.atualizaPosicao();
            clearBlockCount();
            return;
        }

        int oldLin = this.getCelula().getLinha();
        int oldCol = this.getCelula().getColuna();

        int linAlvo = super.getAlvo().getCelula().getLinha();
        int colAlvo = super.getAlvo().getCelula().getColuna();

        int lin = oldLin;
        int col = oldCol;
        if(lin < linAlvo) lin++;
        if(lin > linAlvo) lin--;
        if(col > colAlvo) col--;
        if(col < colAlvo) col++;


        if (lin < 0) lin = 0;
        if (lin >= Jogo.NLIN) lin = Jogo.NLIN-1;
        if (col < 0) col = 0;
        if (col >= Jogo.NCOL) col = Jogo.NCOL-1;

        if (Jogo.getInstance().getCelula(lin, col).getPersonagem() != null){
            super.incrBlockCount();
            return;
        }else{
            // Limpa celula atual
            Jogo.getInstance().getCelula(oldLin, oldCol).setPersonagem(null);
            // Coloca personagem na nova posição
            Jogo.getInstance().getCelula(lin, col).setPersonagem(this);
            super.clearBlockCount();
        }
    }

    @Override
    public void influenciaVizinhos() {
        if(!estaVivo()){
            return;
        }
        if(getAlvo()!=null&&getDistancia(getAlvo())<2){ //true se esta em uma celula adjacente
            if(diceRoller(chanceHit)){
                System.out.println("ZumbiEsperto ("+this.getCelula().getLinha()+","+this.getCelula().getColuna()+") acertou " +getAlvo().getClass().getName()+" ("+getAlvo().getCelula().getLinha()+","+getAlvo().getCelula().getColuna()+")");
                getAlvo().diminuiEnergia(2);
                if(diceRoller(getChanceInfecta())){
                    System.out.println("ZumbiEsperto ("+this.getCelula().getLinha()+","+this.getCelula().getColuna()+") infectou " +getAlvo().getClass().getName()+" ("+getAlvo().getCelula().getLinha()+","+getAlvo().getCelula().getColuna()+")");
                    getAlvo().infecta();
                }
            }      
        }
    }
}