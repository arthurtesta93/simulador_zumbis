public class Zumbi extends Personagem {
    private static int chanceInfecta = 50;
    public Zumbi(int linInicial,int colInicial){
        super(10,"Zumbi",linInicial,colInicial);
    }
    public Zumbi(String name, int linInicial,int colInicial){
        super(10, name ,linInicial,colInicial);
    }

    public Zumbi(int pv, String name, int linInicial,int colInicial){
        super(pv, name ,linInicial,colInicial);
    }

    /**
     * @return the chanceInfecta
     */
    public int getChanceInfecta() {
        return chanceInfecta;
    }

    public void setChanceInfecta(int novaChance) {
        chanceInfecta = novaChance;
    }

    @Override
    public void defineAlvo() {
        // Nao define alvo, eh aleatorio
    }

    @Override
    public void influenciaVizinhos() {
        if(!estaVivo()){
            return;
        }
        int lin = this.getCelula().getLinha();
        int col = this.getCelula().getColuna();
        for(int l=lin-1;l<=lin+1;l++){
            for(int c=col-1;c<=col+1;c++){
                // Se a posição é dentro do tabuleiro
                if (l>=0 && l<Jogo.NLIN && c>=0 && c<Jogo.NCOL){
                    // Se não é a propria celula
                    if (!( lin == l && col == c)){
                        // Recupera o personagem da célula vizinha
                        Personagem p = Jogo.getInstance().getCelula(l,c).getPersonagem();
                        // Se não for nulo, infecta
                        if (p != null && p instanceof Bobao){
                            if(super.diceRoller(chanceInfecta)){
                                System.out.println("Zumbi ("+lin+","+col+") infectou " +p.getClass().getName()+" ("+l+","+c+")");
                                p.infecta();
                            }
                            
                        }
                    }
                }
            }
        }
    }
}